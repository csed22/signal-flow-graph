package gui;

import java.io.File;
import java.net.MalformedURLException;

import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graphview.SmartCircularSortedPlacementStrategy;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;
import com.brunomnsilva.smartgraph.graphview.SmartPlacementStrategy;

import algorithm.dataStructures.Link;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import main.SavedExample;

public class GUIBuilder {

	static GraphManger<? extends Link> graphManger;

	public static <V extends Link> SmartGraphPanel<String, V> layoutScene(Stage primaryStage, Graph<String, V> g) {

		SmartPlacementStrategy strategy = new SmartCircularSortedPlacementStrategy();
		SmartGraphPanel<String, V> graphView = new SmartGraphPanel<>(g, strategy);
		graphManger = new GraphManger<>(graphView, g);

		Button[] buttons = new Button[3];

		buttons[0] = new Button("Insert");
		buttons[1] = new Button("Find Gain");
		buttons[2] = new Button("Random Example");

		GUIBuilder.viewStage(primaryStage, combine(graphView, buttons, g));

		return graphView;
	}

	private static <V extends Link> Scene combine(SmartGraphPanel<String, V> graphView, Button[] buttons,
			Graph<String, V> g) {
		// adjusting the layout
		// stackoverflow.com/questions/44650084/how-to-add-and-align-button-at-center-below-the-graph-with-controller-file-in-ja/44675576
		StackPane spLineChart = new StackPane();
		spLineChart.getChildren().add(graphView);

		// Using Buttons to Manager Chart Data
		// https://docs.oracle.com/javafx/2/charts/scatter-chart.htm
		final HBox hbox = new HBox();

		buttons[0].setOnMouseClicked((event) -> {
			graphManger.insert();
		});

		buttons[1].setOnMouseClicked((event) -> {
			graphManger.findGain();
		});

		buttons[2].setOnMouseClicked((event) -> {
			SavedExample.run(graphView, g);
		});

		hbox.setSpacing(10);
		hbox.getChildren().addAll(buttons);
		hbox.setPadding(new Insets(10, 0, 10, 0));

		// Positioning the button
		// https://stackoverflow.com/questions/27648860/how-to-position-a-button-in-hbox-javafx
		hbox.setAlignment(Pos.CENTER);

		StackPane spButton = new StackPane();
		spButton.getChildren().add(hbox);

		VBox vbox = new VBox();
		VBox.setVgrow(spLineChart, Priority.ALWAYS); // Make line chart always grow vertically
		vbox.getChildren().addAll(spLineChart, spButton);

		return new Scene(vbox, 800, 600);
	}

	private static void viewStage(Stage primaryStage, Scene scene) {
		try {
			setIcon(primaryStage);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		primaryStage.setTitle("Signal Flow Graph Visualization");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	@SuppressWarnings("deprecation")
	private static void setIcon(Stage primaryStage) throws MalformedURLException {
		// Add a custom icon.
		// https://stackoverflow.com/questions/6098472/pass-a-local-file-in-to-url-in-java
		primaryStage.getIcons().add(new Image(new File("imgs/icon.png").toURL().toString()));
	}

}
