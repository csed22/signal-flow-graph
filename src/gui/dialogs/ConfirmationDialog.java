package gui.dialogs;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;

public class ConfirmationDialog extends AbstractDialog {

	String[] buttons;

	public ConfirmationDialog(String title, String content, String[] buttons) {
		super(title, content);
		this.buttons = buttons;
	}

	public void run() {

		Alert alert = getAlertBody();

		int n = buttons.length;
		ButtonType[] buttonArr = new ButtonType[n + 1];

		for (int i = 0; i < n; i++) {
			buttonArr[i] = new ButtonType(buttons[i]);
		}

		buttonArr[n] = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		alert.getButtonTypes().setAll(buttonArr);

		Optional<ButtonType> result = alert.showAndWait();

		if (result.isPresent()) {
			for (ButtonType button : buttonArr) {
				if (result.get() == button) {
					returnValue = button.getText();
					return;
				}
			}
		}
		
		returnValue = "";
		
	}

}
