package gui.dialogs;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Collection;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;

// https://code.makery.ch/blog/javafx-dialogs-official/
abstract class AbstractDialog {

	final String title;
	final String content;
	String returnValue;

	public AbstractDialog(String title, String content) {
		returnValue = "";
		this.title = title;
		this.content = content;
	}

	TextInputDialog getInputBody(String defaultValue) {
		TextInputDialog dialog = new TextInputDialog(defaultValue);
		utilize(dialog);
		return dialog;
	}

	Alert getAlertBody() {
		Alert dialog = new Alert(AlertType.CONFIRMATION);
		utilize(dialog);
		return dialog;
	}

	<T> ChoiceDialog<T> getChoicesBody(T defaultChoice, Collection<T> choices) {
		ChoiceDialog<T> dialog = new ChoiceDialog<>(defaultChoice, choices);
		utilize(dialog);
		return dialog;
	}

	public abstract void run();

	public String getReturnValue() {
		return returnValue;
	}

	private <T> void utilize(Dialog<T> dialog) {
		try {
			setIcon(dialog);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		dialog.setTitle("Signal Flow Graph");
		dialog.setHeaderText(title);
		dialog.setContentText(content);
	}

	@SuppressWarnings("deprecation")
	private static <T> void setIcon(Dialog<T> dialog) throws MalformedURLException {
		// Get the Stage.
		Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
		// Add a custom icon.
		// https://stackoverflow.com/questions/6098472/pass-a-local-file-in-to-url-in-java
		stage.getIcons().add(new Image(new File("imgs/icon.png").toURL().toString()));
	}

}
