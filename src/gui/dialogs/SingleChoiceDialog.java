package gui.dialogs;

import java.util.Collection;
import java.util.Optional;

import javafx.scene.control.ChoiceDialog;

public class SingleChoiceDialog<T> extends AbstractDialog {

	Collection<T> choices;

	public SingleChoiceDialog(String title, String content, Collection<T> choices) {
		super(title, content);
		this.choices = choices;
	}

	public void run() {

		ChoiceDialog<T> dialog = getChoicesBody(null, choices);

		// Traditional way to get the response value.
		Optional<T> result = dialog.showAndWait();
		if (result.isPresent()) {
			returnValue = result.get().toString();
			return;
		}

		returnValue = "";
	}
	
}
