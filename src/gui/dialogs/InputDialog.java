package gui.dialogs;

import java.util.Optional;

import javafx.scene.control.TextInputDialog;

public class InputDialog extends AbstractDialog {

	public InputDialog(String title, String content) {
		super(title, content);
	}

	public void run() {

		TextInputDialog dialog = getInputBody(null);

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			returnValue = result.get();
			return;
		}

		returnValue = "";
	}
}
