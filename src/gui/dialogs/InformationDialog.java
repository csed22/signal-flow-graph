package gui.dialogs;

import javafx.scene.control.Alert;

public class InformationDialog extends AbstractDialog {

	public InformationDialog(String title, String content) {
		super(title, content);
	}

	public void run() {
		Alert alert = getAlertBody();
		alert.showAndWait();
	}

}
