package gui;

import java.rmi.AlreadyBoundException;
import java.util.Timer;
import java.util.TimerTask;

import org.omg.CosNaming.NamingContextPackage.NotFound;

import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graph.Vertex;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;

import algorithm.AlgorithmsManger;
import algorithm.dataStructures.Link;
import algorithm.dataStructures.LinksManger;
import algorithm.dataStructures.NodesManger;
import files.FileWriter;
import gui.dialogs.ConfirmationDialog;
import gui.dialogs.InformationDialog;
import gui.dialogs.InputDialog;
import gui.dialogs.SingleChoiceDialog;

public class GraphManger<V extends Link> {

	private Graph<String, V> g;
	private SmartGraphPanel<String, V> graphView;

	public GraphManger(SmartGraphPanel<String, V> graphView, Graph<String, V> g) {
		this.graphView = graphView;
		this.g = g;
	}

	public void insert() {
		String confimTitle = "Insertion Types";
		String confimContent = "Choose:";
		String[] confimOptions = { "Vertex", "Edge" };
		ConfirmationDialog confirmDialog = new ConfirmationDialog(confimTitle, confimContent, confimOptions);
		confirmDialog.run();

		switch (confirmDialog.getReturnValue()) {
		case "Vertex":
			insertVertex();
			break;
		case "Edge":
			insertEdge();
			break;
		default:
		}
	}

	private void insertVertex() {
		String inputTitle = "Insertion Dialog";
		String inputeContent = "Vertex name:";
		InputDialog input = new InputDialog(inputTitle, inputeContent);
		input.run();
		String vertex = input.getReturnValue();
		if (!entersNothing(vertex)) {

			try {
				vertex = NodesManger.getVertex(vertex);
			} catch (AlreadyBoundException e) {
				System.out.println("Already here.");
				return;
			}

			g.insertVertex(vertex);
			updateGraph();

			if (counterFlag < 2) {
				cleaningProcedures();
			}

		}

	}

	private int counterFlag = 0;

	private void cleaningProcedures() {
		TimerTask reloading = new TimerTask() {
			// https://stackoverflow.com/questions/12865803/how-do-i-refresh-a-gui-in-java
			public void run() {
				String condition = null;
				if (counterFlag == 0) {
					condition = "";
				}
				if (counterFlag == 1) {
					condition = "Hey, Please drag me before you start ..";
				}
				for (Vertex<String> v : g.vertices()) {
					if (condition != null && condition.equals(v.element())) {
						g.removeVertex(v);
						System.out.println("Deleted " + v.element());
					}
				}
				counterFlag++;
				cancel();
			}
		};
		Timer clearTimer = new Timer();
		clearTimer.scheduleAtFixedRate(reloading, 200, 100);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void insertEdge() {
		String choiceTitle = "Insertion Dialog";
		String choiceContent = "From:";
		SingleChoiceDialog<?> outBound = new SingleChoiceDialog(choiceTitle, choiceContent, NodesManger.getNodeList());
		outBound.run();
		String from = outBound.getReturnValue();
		if (entersNothing(from)) {
			return;
		}
		choiceTitle = "Insertion Dialog";
		choiceContent = "To:";
		SingleChoiceDialog<?> inBound = new SingleChoiceDialog(choiceTitle, choiceContent, NodesManger.getNodeList());
		inBound.run();
		String to = inBound.getReturnValue();
		if (entersNothing(to)) {
			return;
		}
		String inputTitle = "Insertion Dialog";
		String inputeContent = "Edge weight:";
		InputDialog input = new InputDialog(inputTitle, inputeContent);
		input.run();
		String value = input.getReturnValue();
		if (entersNothing(value)) {
			return;
		}

		Link edge;
		try {
			edge = LinksManger.getLink(from, to, value);
		} catch (AlreadyBoundException e) {
			System.out.println("Already here.");
			return;
		}

		if (edge != null) {
			g.insertEdge(from, to, (V) edge);
			updateGraph();
		}
	}

	private boolean entersNothing(String returnValue) {
		if (returnValue.equals(""))
			return true;
		return false;
	}

	public void findGain() {
		String infoTitle;
		String infoContent;
		try {
			AlgorithmsManger manger = new AlgorithmsManger(NodesManger.getIdsList(), LinksManger.getMap());
			infoTitle = "Using Mason Gain Formula";

			FileWriter fileManger = new FileWriter("solution.txt");
			fileManger.write(manger.solve());
			infoContent = "The answer is " + manger.finalAnswer()
					+ "\nPlease check solution.txt file for further information.";
		} catch (NotFound e) {
			infoTitle = "Error!";
			infoContent = "Please make sure that the graph has only one source and only one sick node.";
		}
		new InformationDialog(infoTitle, infoContent).run();
	}

	private void updateGraph() {
		graphView.update();
//		graphView.setAutomaticLayout(true);
//		if (sec < 0)
//			return;
//		stopMoving(sec * 1000);
	}

	@SuppressWarnings("unused")
	private void stopMoving(long secs) {
		TimerTask reloading = new TimerTask() {
			// https://stackoverflow.com/questions/12865803/how-do-i-refresh-a-gui-in-java
			public void run() {
				graphView.setAutomaticLayout(false);
				cancel();
			}
		};
		Timer closeTimer = new Timer();
		closeTimer.scheduleAtFixedRate(reloading, secs, 100);
	}

}
