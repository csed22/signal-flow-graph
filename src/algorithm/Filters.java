package algorithm;

import java.util.ArrayList;

import algorithm.NonTouchingTable.Entity;

public class Filters {

	static <T> void filterTheSimilar(ArrayList<T> collection) {
		int n = collection.size();
		for (int i = 0; i < n; i++) {
			T e = collection.get(i);
			ArrayList<Integer> indexes = new ArrayList<>();
			for (int j = i + 1; j < n; j++) {
				if (theyAreSimilar(e, collection.get(j))) {
					indexes.add(j);
				}
			}
			for (int d = indexes.size() - 1; d >= 0; d--) {
				int index = indexes.get(d);
				collection.remove(index);
			}
			n = collection.size();
		}
	}

	static <T> boolean theyAreSimilar(T e1, T e2) {

		// meant to face the problem of (1,2) == (2,1)
		if (e1 instanceof Entity) {
			for (Loop l_1 : ((Entity) e1).group) {
				boolean matchFound = false;
				for (Loop l_2 : ((Entity) e2).group) {
					if (l_1.toString().equals(l_2.toString()))
						matchFound = true;
				}
				if (!matchFound)
					return false;
			}
		}

		// meant to face the problem of (5->6->5) == (6->5->6)
		if (e1 instanceof Loop) {
			ArrayList<Character> c1 = ((Loop) e1).path;
			ArrayList<Character> c2 = ((Loop) e2).path;

			if (c1.containsAll(c2) && c2.containsAll(c1))
				return true;

			return false;

		}

		return true;
	}

}
