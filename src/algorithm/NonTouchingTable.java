package algorithm;

import java.util.ArrayList;

public class NonTouchingTable {

	ArrayList<Entity>[] table;

	@SuppressWarnings("unchecked")
	public NonTouchingTable(ArrayList<Loop> loops, int n) {
		table = (ArrayList<Entity>[]) new ArrayList<?>[n];
		fillOutTable(loops, 1);
//		System.out.println(Arrays.deepToString(table));
	}

	private void fillOutTable(ArrayList<Loop> loops, int rank) {

		table[rank - 1] = new ArrayList<>();

		if (rank == 1) {
			for (Loop l : loops) {
				table[rank - 1].add(new Entity(l));
			}
		} else if (rank >= table.length) {
			return;

		} else {

			for (Loop l : loops) {
				for (Entity e : table[rank - 2]) {
					if (e.anyIntersections(l))
						continue;
					table[rank - 1].add(e.upgrade(l));
				}
			}

			Filters.filterTheSimilar(table[rank - 2]);

		}

		fillOutTable(loops, ++rank);

	}

	@Override
	public String toString() {
		StringBuilder strBuilder = new StringBuilder();

		for (int i = 1; i < table.length; i++) {
			strBuilder.append("\n" + (i + 1) + " Non Touching\n");
			for (Entity e : table[i]) {
				strBuilder.append(e);
			}
		}

		return strBuilder.toString();
	}

	class Entity {

		int rank;
		ArrayList<Loop> group;

		Entity(Loop l) {
			this.rank = 1;
			group = new ArrayList<>();
			group.add(l);
		}

		boolean anyIntersections(Path oneMore) {
			for (Path p : group) {
				if (p.doesItTouch(oneMore))
					return true;
			}
			return false;
		}

		Entity upgrade(Loop oneMore) {
			Entity e = new Entity();
			e.rank = this.rank + 1;
			for (Loop l : group) {
				e.group.add(l);
			}
			e.group.add(oneMore);
			return e;
		}

		private Entity() {
			group = new ArrayList<>();
		}

		@Override
		public String toString() {
			StringBuilder strBuilder = new StringBuilder(/* "<" + rank + "> " */);

			for (Loop l : group)
				strBuilder.append(l + ", ");
			strBuilder.deleteCharAt(strBuilder.length() - 2);

			strBuilder.append("\n");

			return strBuilder.toString();
		}

	}

}
