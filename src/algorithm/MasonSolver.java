package algorithm;

import java.util.ArrayList;

import algorithm.NonTouchingTable.Entity;

public class MasonSolver {

	Traverser t;
	int[][] matrix;

	public MasonSolver(Traverser t, int[][] matrix) {
		this.t = t;
		this.matrix = matrix;
	}

	double finalAnswer = 0.0;

	String solutionReport() {
		StringBuilder strBuilder = new StringBuilder();

		ArrayList<Loop> loops = new ArrayList<>();
		ArrayList<Path> forwardPaths = new ArrayList<>();
		seperateLoops(t.paths, loops, forwardPaths);
		Filters.filterTheSimilar(loops);

		strBuilder.append("Forward Paths: " + forwardPaths + "\n");
		strBuilder.append("Loops: " + loops + "\n");

		NonTouchingTable cantTouchThis = new NonTouchingTable(loops, matrix.length);
		strBuilder.append("\nNon Touching Loops Table:\n" + cantTouchThis + "\n");

		GainCalculator calculator = new GainCalculator(cantTouchThis, matrix);

		int n = forwardPaths.size();
		long[] fGains = new long[n];
		strBuilder.append("Forward Paths Gains:\n");
		for (int i = 0; i < n; i++) {
			Path p = forwardPaths.get(i);
			fGains[i] = calculator.calcPathGain(p);
			strBuilder.append(p + " = " + fGains[i] + "\n");
		}

		long[] deltas = calculateDeltas(forwardPaths, calculator);

		strBuilder.append("\nGraph determinant (Delta) = " + deltas[n] + "\n");

		for (int i = 0; i < n; i++) {
			strBuilder.append("Delta_" + (i + 1) + " = " + deltas[i] + "\n");
		}

		finalAnswer = sumThemAll(fGains, deltas);

		strBuilder.append("\nThe Final Answer is " + finalAnswer + "\n");

		return strBuilder.toString();
	}

	private void seperateLoops(ArrayList<Path> paths, ArrayList<Loop> loops, ArrayList<Path> forwardPaths) {
		for (Path p : paths)
			if (p instanceof Loop)
				loops.add((Loop) p);
			else
				forwardPaths.add(p);
	}

	private long[] calculateDeltas(ArrayList<Path> forwardPaths, GainCalculator calculator) {
		int n = forwardPaths.size();
		long[] values = new long[n + 1];

		long theBigDelta = 1;

		for (Entity e : calculator.calculationsTable.keySet()) {
			long loopsGain = calculator.calculationsTable.get(e);
			if (e.rank % 2 == 0) {
//				System.out.println(e + " Add " + loopsGain);
				theBigDelta += loopsGain;
			} else {
//				System.out.println(e + " Sub " + loopsGain);
				theBigDelta -= loopsGain;
			}
		}

		values[n] = theBigDelta;
//		System.out.println(theBigDelta);

		for (int i = 0; i < n; i++) {
			values[i] = 1;
			Path p = forwardPaths.get(i);
			for (Entity e : calculator.calculationsTable.keySet()) {
				if (e.anyIntersections(p))
					continue;
				long loopsGain = calculator.calculationsTable.get(e);
				if (e.rank % 2 == 0) {
//					System.out.println(e + " Add " + loopsGain);
					values[i] += loopsGain;
				} else {
//					System.out.println(e + " Sub " + loopsGain);
					values[i] -= loopsGain;
				}

			}
		}

		return values;
	}

	private double sumThemAll(long[] fGains, long[] deltas) {
		double result = 0.0;

		int n = fGains.length;
		for (int i = 0; i < n; i++) {
			result += (fGains[i] * deltas[i]);
		}
		result /= (double) deltas[n];

		return result;
	}

}
