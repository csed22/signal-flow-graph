package algorithm.dataStructures;

public class Link {

	final char[] id;
	final int value;

	Link(char from, char to, int value) {
		this.id = getId(from, to);
		this.value = value;
	}

	public String toString() {
		return Integer.toString(value);
	}

	private char[] getId(char from, char to) {
		char[] _id = { from, to };
		return _id;
	}

}
