package algorithm.dataStructures;

import java.rmi.AlreadyBoundException;
import java.util.HashMap;
import java.util.Map;

import org.omg.CosNaming.NamingContextPackage.NotFound;

public abstract class LinksManger {

	static Map<String, Integer> linksList = new HashMap<>();

	public static Link getLink(String fromName, String toName, String weight) throws AlreadyBoundException {
		char from, to;
		try {
			from = NodesManger.findNode(fromName);
			to = NodesManger.findNode(toName);
		} catch (NotFound e) {
			System.out.println("Can't form a link!");
			return null;
		}
		Link l = null;
		Integer w = toInt(weight);
		if (w != null) {
			l = new Link(from, to, w);
			int oldSize = linksList.size();
			linksList.put(new String(l.id), w);
			if (oldSize < linksList.size())
				return l;
			else
				throw new AlreadyBoundException();
		}
		return l;
	}

	public static Map<String, Integer> getMap() {
		return linksList;
	}

	private static Integer toInt(String weight) {
		Integer num = null;
		try {
			num = Integer.parseInt(weight);
		} catch (NumberFormatException e) {
			System.out.println("Please, enter a number!!");
		}
		return num;
	}

	public static void forget() {
		linksList = new HashMap<>();
	}

}
