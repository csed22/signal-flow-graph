package algorithm.dataStructures;

import static algorithm.AlgorithmsManger.NUMBERING_START;

import java.rmi.AlreadyBoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import org.omg.CosNaming.NamingContextPackage.NotFound;

public abstract class NodesManger {

	static Map<String, Character> nodeList = new HashMap<>();

	public static String getVertex(String node) throws AlreadyBoundException {
		char id = generateKey();
		int oldSize = nodeList.size();
		nodeList.put(node, id);
		if (oldSize < nodeList.size())
			return node;
		else
			throw new AlreadyBoundException();
	}

	static char findNode(String node) throws NotFound {
		if (nodeList.get(node) != null)
			return nodeList.get(node);
		else
			throw new NotFound();
	}

	public static String findNodeName(char c) {
		return getKeyByValue(nodeList, c);
	}

	// https://stackoverflow.com/questions/1383797/java-hashmap-how-to-get-key-from-value
	private static <T, E> T getKeyByValue(Map<T, E> map, E value) {
		for (Entry<T, E> entry : map.entrySet()) {
			if (Objects.equals(value, entry.getValue())) {
				return entry.getKey();
			}
		}
		return null;
	}

	public static Collection<Character> getIdsList() {
		return nodeList.values();
	}

	public static Collection<String> getNodeList() {
		return nodeList.keySet();
	}

	private static char keyCounter = NUMBERING_START;

	private static char generateKey() {
		if (keyCounter <= 'z')
			return keyCounter++;
		else
			throw new ArrayIndexOutOfBoundsException();
	}

	public static void forget() {
		keyCounter = NUMBERING_START;
		nodeList = new HashMap<>();
	}

}
