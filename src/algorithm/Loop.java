package algorithm;

import java.util.ArrayList;

import algorithm.dataStructures.NodesManger;

public class Loop extends Path {

	Loop(Path p) {

		ArrayList<Character> fullPath = p.path;
		path = new ArrayList<>();

		int n = fullPath.size();
		char doublicate = fullPath.get(n - 1);
		int firstOccur = fullPath.indexOf(doublicate);

		for (int i = firstOccur; i < n; i++) {
			path.add(fullPath.get(i));
		}

	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		for (int i = 0; i < path.size() - 1; i++) {
			char c = path.get(i);
			strBuilder.append(NodesManger.findNodeName(c) + " <-> ");
		}
		return "(" + strBuilder.substring(0, strBuilder.length() - 5) + ")";
	}

}
