package algorithm;

import java.util.ArrayList;

import algorithm.dataStructures.NodesManger;

public class Path {

	ArrayList<Character> path;

	Path() {
		path = new ArrayList<>();
	}

	Path(Path p) {
		this();
		for (char c : p.path) {
			path.add(c);
		}
	}

	boolean addNode(char c) {
		boolean loop = didItPass(c);
		path.add(c);

		if (loop)
			return false;
		return true;
	}

	boolean didItPass(char c) {
		return path.contains(c);
	}

	boolean doesItTouch(Path p) {
		for (char c_in : path) {
			for (char c_out : p.path) {
				if (c_in == c_out)
					return true;
			}
		}
		return false;
	}

	public String toString() {
		StringBuilder strBuilder = new StringBuilder();
		for (char c : path) {
			strBuilder.append(NodesManger.findNodeName(c) + "-> ");
		}
		return strBuilder.substring(0, strBuilder.length() - 3);
	}

}
