package algorithm;

import static algorithm.AlgorithmsManger.NUMBERING_START;

import java.util.ArrayList;

import org.omg.CosNaming.NamingContextPackage.NotFound;

public class GraphList {

	public final char source;
	public final char sink;

	private int n; // size of the matrix
	private ArrayList<Character>[] adjMatrix;

	GraphList(int[][] base) throws NotFound {
		this.n = base.length;
		createFirstLayer(base);

		source = identifySource(adjMatrix);
		sink = identifySink(adjMatrix);

//		System.out.println(source + ", " + sink);
	}

	Traverser getTraverser() {
		return new Traverser(this);
	}

	ArrayList<Character> getAvailablePaths(char row) {
		row -= (NUMBERING_START);
		return adjMatrix[row];
	}

	@SuppressWarnings("unchecked")
	private void createFirstLayer(int[][] base) {
		adjMatrix = (ArrayList<Character>[]) new ArrayList<?>[n];
		for (int i = 0; i < n; i++) {
			adjMatrix[i] = new ArrayList<>();
			for (int j = 0; j < n; j++) {
				if (base[i][j] != 0) {
					adjMatrix[i].add(getIDChar(j));
				}
			}
		}
	}

	private char identifySource(ArrayList<Character>[] m) throws NotFound {
		char foundIndex = 0;
		boolean foundMore = false;

		for (int j = 0; j < n; j++) {
			boolean approved = true;
			for (int i = 0; i < n; i++) {
				if (m[i].contains(getIDChar(j))) {
					approved = false;
					break;
				}
			}
			if (approved) {
				if (foundIndex > 0)
					foundMore = true;
//				System.out.println(getIDChar(j) + " is approved as source !!");
				foundIndex += getIDChar(j);
			}
		}

		if (foundIndex > 0) {
			if (foundMore)
				throw new NotFound();
			else
				return foundIndex;
		}
		throw new NotFound();
	}

	private char identifySink(ArrayList<Character>[] m) throws NotFound {
		char foundIndex = 0;
		boolean foundMore = false;

		for (int i = 0; i < n; i++) {
			if (m[i].size() == 0) {
				if (foundIndex > 0)
					foundMore = true;
				foundIndex += getIDChar(i);
//				System.out.println(getIDChar(i) + " is approved as sink !!");
			}
		}

		if (foundIndex > 0) {
			if (foundMore)
				throw new NotFound();
			else
				return foundIndex;
		}
		throw new NotFound();
	}

	public String toString() {

		StringBuilder strBuilder = new StringBuilder("");

		for (int i = 0; i < n; i++) {
			strBuilder.append(getIDChar(i) + ": " + adjMatrix[i] + "\n");
		}

		return strBuilder.toString();
	}

	private static char getIDChar(int i) {
		return (char) (NUMBERING_START + i);
	}

}