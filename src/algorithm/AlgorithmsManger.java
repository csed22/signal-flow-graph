package algorithm;

import java.util.Collection;
import java.util.Map;

import org.omg.CosNaming.NamingContextPackage.NotFound;

public class AlgorithmsManger {

	public static final char NUMBERING_START = 'A';
	GraphList paths;
	int[][] matrix;

	public AlgorithmsManger(Collection<Character> points, Map<String, Integer> edges) throws NotFound {
		int n = points.size();
		matrix = new int[n][n];

		fillOutTheMatrix(edges);
		paths = new GraphList(matrix);
	}

	private void fillOutTheMatrix(Map<String, Integer> edges) {
		for (Map.Entry<String, Integer> entry : edges.entrySet()) {
			char[] ids = entry.getKey().toCharArray();
			ids[0] -= NUMBERING_START;
			ids[1] -= NUMBERING_START;
			matrix[ids[0]][ids[1]] = entry.getValue();
		}
	}

	MasonSolver solver;

	public String solve() {
		Traverser t = paths.getTraverser();
		solver = new MasonSolver(t, matrix);
		return solver.solutionReport();
	}

	public double finalAnswer() {
		return solver.finalAnswer;
	}

}
