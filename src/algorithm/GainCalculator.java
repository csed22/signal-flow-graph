package algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import algorithm.NonTouchingTable.Entity;
import static algorithm.AlgorithmsManger.NUMBERING_START;

public class GainCalculator {

	int[][] matrix;
	Map<Entity, Long> calculationsTable;

	public GainCalculator(NonTouchingTable cantTouchThis, int[][] matrix) {

		this.matrix = matrix;
		calculationsTable = new HashMap<>();

		for (ArrayList<Entity> eArr : cantTouchThis.table) {
			for (Entity e : eArr) {
				calculationsTable.put(e, calcEntity(e));
			}
		}

		System.out.println(calculationsTable);

	}

	private Long calcEntity(Entity e) {
		long totalVal = 1;
		for (Loop l : e.group) {
			totalVal *= calcPathGain(l);
		}
		return totalVal;
	}

	public long calcPathGain(Path l) {
		long totalVal = 1;
		for (int i = 1; i < l.path.size(); i++) {
			int c_source = l.path.get(i - 1);
			int c_destination = l.path.get(i);
			totalVal *= getIndexValue(c_source, c_destination);
		}
		return totalVal;
	}

	private long getIndexValue(int c_source, int c_destination) {
		return matrix[c_source - NUMBERING_START][c_destination - NUMBERING_START];
	}

}
