package algorithm;

import java.util.ArrayList;

public class Traverser {

	GraphList map;
	ArrayList<Path> paths;

	public Traverser(GraphList map) {
		this.map = map;
		init();
	}

	void init() {
		paths = new ArrayList<>();
		Path p = new Path();
		addRecursive(map.source, p);
//		System.out.println(paths);
	}

	private void addRecursive(char source, Path prev) {
		Path p = new Path(prev);

		if (!p.addNode(source)) {
			paths.add(new Loop(p));
			return;
		}

		if (map.getAvailablePaths(source).isEmpty()) {
			paths.add(p);
			return;
		}

		for (char c : map.getAvailablePaths(source)) {
			addRecursive(c, p);
		}

	}

}
