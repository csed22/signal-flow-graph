package main;

import com.brunomnsilva.smartgraph.graph.Digraph;
import com.brunomnsilva.smartgraph.graph.DigraphEdgeList;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;

import algorithm.dataStructures.Link;
import gui.GUIBuilder;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainClass extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		Digraph<String, Link> g = new DigraphEdgeList<>();
		SmartGraphPanel<String, Link> graphView = GUIBuilder.layoutScene(primaryStage, g);

		// IMPORTANT - Called after scene is displayed so we can have width and height
		// values
		graphView.setAutomaticLayout(true);
		graphView.init();

		g.insertVertex("");
		g.insertVertex("Hey, Please drag me before you start ..");
		
		graphView.update();
	}

}
