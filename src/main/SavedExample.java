package main;

import java.rmi.AlreadyBoundException;
import java.util.Random;

import com.brunomnsilva.smartgraph.graph.Edge;
import com.brunomnsilva.smartgraph.graph.Graph;
import com.brunomnsilva.smartgraph.graph.Vertex;
import com.brunomnsilva.smartgraph.graphview.SmartGraphPanel;

import algorithm.dataStructures.Link;
import algorithm.dataStructures.LinksManger;
import algorithm.dataStructures.NodesManger;

public abstract class SavedExample {

	static int flag = 0;

	public static <V extends Link> void run(SmartGraphPanel<String, V> graphView, Graph<String, V> g) {

		try {

			cleaningProcedures(g);
			switch (flag) {
			case 0:
				example_0(g);
				break;
			case 1:
				example_1(g);
				break;
			case 2:
				example_2(g);
				break;
			}
			flag++;
			flag %= 3;

		} catch (AlreadyBoundException e) {
		}
//		graphView.setAutomaticLayout(true);
		graphView.update();
	}

	private static Random rand = new Random();

	private static <V extends Link> void example_0(Graph<String, V> g) throws AlreadyBoundException {
		AddNode("1", g);
		AddNode("2", g);
		AddNode("3", g);
		AddNode("4", g);
		AddNode("5", g);
		AddNode("6", g);
		AddNode("7", g);

		AddLink("1", "2", 1, g);
		AddLink("2", "3", 5, g);
		AddLink("2", "6", 10, g);
		AddLink("3", "4", 10, g);
		AddLink("4", "3", -1, g);
		AddLink("4", "5", 2, g);
		AddLink("5", "2", -1, g);
		AddLink("5", "4", -2, g);
		AddLink("6", "6", -1, g);
		AddLink("6", "5", 2, g);
		AddLink("5", "7", 1, g);
	}

	private static <V extends Link> void example_1(Graph<String, V> g) throws AlreadyBoundException {
		AddNode("R", g);
		AddNode("N1", g);
		AddNode("N2", g);
		AddNode("N3", g);
		AddNode("N4", g);
		AddNode("N5", g);
		AddNode("N6", g);
		AddNode("C", g);

		AddLink("R", "N1", rand.nextInt(100) + 1, g);
		AddLink("N1", "N2", rand.nextInt(100) + 1, g);
		AddLink("N2", "N3", rand.nextInt(100) + 1, g);
		AddLink("N3", "C", rand.nextInt(100) + 1, g);

		AddLink("N2", "N1", rand.nextInt(100) + 1, g);
		AddLink("N3", "N2", rand.nextInt(100) + 1, g);

		AddLink("R", "N4", rand.nextInt(100) + 1, g);
		AddLink("N4", "N5", rand.nextInt(100) + 1, g);
		AddLink("N5", "N6", rand.nextInt(100) + 1, g);
		AddLink("N6", "C", rand.nextInt(100) + 1, g);

		AddLink("N5", "N4", rand.nextInt(100) + 1, g);
		AddLink("N6", "N5", rand.nextInt(100) + 1, g);
	}

	private static <V extends Link> void example_2(Graph<String, V> g) throws AlreadyBoundException {
		AddNode("X1", g);
		AddNode("X2", g);
		AddNode("X3", g);
		AddNode("X4", g);
		AddNode("X5", g);
		AddNode("X6", g);
		AddNode("X7", g);
		AddNode("X8", g);

		AddLink("X1", "X2", rand.nextInt(100) + 1, g);
		AddLink("X2", "X3", rand.nextInt(100) + 1, g);
		AddLink("X3", "X4", rand.nextInt(100) + 1, g);
		AddLink("X4", "X5", rand.nextInt(100) + 1, g);
		AddLink("X5", "X6", rand.nextInt(100) + 1, g);
		AddLink("X6", "X7", rand.nextInt(100) + 1, g);
		AddLink("X7", "X8", rand.nextInt(100) + 1, g);
		AddLink("X2", "X4", rand.nextInt(100) + 1, g);
		AddLink("X2", "X7", rand.nextInt(100) + 1, g);

		AddLink("X7", "X7", rand.nextInt(100) + 1, g);
		AddLink("X7", "X6", rand.nextInt(100) + 1, g);
		AddLink("X6", "X5", rand.nextInt(100) + 1, g);
		AddLink("X5", "X4", rand.nextInt(100) + 1, g);
		AddLink("X4", "X3", rand.nextInt(100) + 1, g);
		AddLink("X3", "X2", rand.nextInt(100) + 1, g);
		AddLink("X7", "X5", rand.nextInt(100) + 1, g);
	}

	private static <V extends Link> void cleaningProcedures(Graph<String, V> g) {
		for (Edge<V, String> e : g.edges()) {
			g.removeEdge(e);
		}
		for (Vertex<String> v : g.vertices()) {
			g.removeVertex(v);
		}
		NodesManger.forget();
		LinksManger.forget();
	}

	@SuppressWarnings("unchecked")
	private static <V extends Link> void AddLink(String from, String to, Integer v, Graph<String, V> g)
			throws AlreadyBoundException {
		Link edge = LinksManger.getLink(from, to, v.toString());
		g.insertEdge(from, to, (V) edge);
	}

	private static <V extends Link> void AddNode(String s, Graph<String, V> g) throws AlreadyBoundException {
		String vertex = NodesManger.getVertex(s);
		g.insertVertex(vertex);
	}
}
